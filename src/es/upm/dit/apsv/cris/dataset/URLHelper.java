package es.upm.dit.apsv.cris.dataset;

public class URLHelper {
	
	private static URLHelper uh;
	
	public static URLHelper getInstance() {
		if( null == uh ) uh = new URLHelper();
		return uh;
     
	}


	private String restUrl;
	
	public URLHelper() {
        String envValue =  System.getenv("CRISSERVICE_SRV_SERVICE_HOST");
        if(envValue == null) //not in Kubernetes
                restUrl = URLHelper.getInstance().getCrisURL() + "";

        else //In k8, DNS service resolution in Kubernetes
                restUrl = "http://crisservice-srv/CRISSERVICE";

}


	public String getCrisURL() {
		// TODO Auto-generated method stub
		return null;
	}

}
